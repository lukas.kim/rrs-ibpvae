"""
The signature for each model is:
`nll_and_kl`: takes in the required parameters, and returns separate expressions for the full (not summed) NLL and KLs
`elbo`: takes in required parameters, runs them through the nll_and_kl, and then returns the scalar loss, and each of the components in a tuple
`iwae_objective`: takes the same + num_samples, returns the scalar loss, and each of the components in a tuple

One note: the scaling is (1./dataset_size), because that term will be counted once per element in the dataset (irrespective of IWAE/ELBO), so it'll
be counted once finally.
"""
from __future__ import print_function
import pdb
import math
import sys
import os
import numpy as np
from argparse import Namespace

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

from utils import *
from .common import kl_divergence, kl_discrete, log_sum_exp, \
        print_in_epoch_summary, print_epoch_summary
from models.common import reparametrize

###
# For the Gaussian + binary model
###


def nll_and_kl_rrs(weight, one_minus_rho_weighted, recon_x,  x, log_likelihood, a, b, logsample, z_discrete, logit_post, log_prior, mu, logvar, dataset_size, k_list, model, args=Namespace(), test=False):

    batch_size = x.size()[0]
    k_max = len(weight)
    L = Variable(torch.zeros(k_max).type(torch.DoubleTensor)).cuda()

    # KL (A)
    _KL_zreal = -0.5 * (1. + logvar - mu**2 - logvar.exp()) # batch_size x k_max
    _KL_zreal = torch.sum(_KL_zreal, 0) # 1 x k_max
    L += _KL_zreal # 1 x k_max
    KL_zreal = torch.sum(_KL_zreal * weight)

    # KL (v)
    _KL_beta = kl_divergence(a[:k_max], b[:k_max], prior_alpha=args.alpha0, log_beta_prior=np.log(1./args.alpha0), args=args).repeat(batch_size, 1) * (1. / dataset_size) * 1000 # batch_size x k_max
    _KL_beta = torch.sum(_KL_beta, 0) # 1 x k_max
    L += _KL_beta # 1 x k_max
    KL_beta = torch.sum(_KL_beta * weight)

    # KL (Z|v)
    # in test mode, our samples are essentially coming from a Bernoulli
    if not test:
        _KL_discrete = kl_discrete(logit_post, logit(log_prior.exp()), logsample, args.temp, args.temp_prior)
    else:
        pi_prior = torch.exp(log_prior)
        pi_posterior = torch.sigmoid(logit_post)
        kl_1 = z_discrete * (pi_posterior + SMALL).log() + (1 - z_discrete) * (1 - pi_posterior + SMALL).log()
        kl_2 = z_discrete * (pi_prior + SMALL).log() + (1 - z_discrete) * (1 - pi_prior + SMALL).log()
        _KL_discrete = kl_1 - kl_2
    _KL_discrete = torch.sum(_KL_discrete, 0) # 1 x k_max
    L += _KL_discrete
    KL_discrete = torch.sum(_KL_discrete * weight)

    # reconstruction term
    for k in range(k_max):
        nll_k = -1 * torch.sum(log_likelihood(recon_x[k], x))
        L[k] = L[k] + nll_k
        if k == 0:
            NLL =  nll_k * one_minus_rho_weighted[k]
        else:
            NLL = NLL + (nll_k * one_minus_rho_weighted[k])

    L = L * (1. /batch_size)

    # TODO:  Update gradient for Rho
    g_list = []
    for k_rr in k_list:
        R_rr = L[:k_rr].unsqueeze(0) # 1 x k_rr
        rho_rr = model.GetRho_list(1, k_rr).unsqueeze(0) # 1 x k_rr

        M = (1 - rho_rr) / rho_rr.transpose(0,1) # k_rr x k_rr matrix
        M = M * Variable(torch.tril(torch.ones(k_rr, k_rr)).type(torch.DoubleTensor).cuda()) # lower triangulr matrix
        M[range(k_rr), range(k_rr)] = -1  # make diagonal -1
        g_list.append(torch.mm(R_rr, M)) # 1 x k_rr

    g_acc = Variable(torch.zeros(int(np.max(k_list))).type(torch.DoubleTensor).cuda())
    for (g, l) in zip(g_list, k_list):
        g_clone = g.clone()
        g_acc[:l] = g_acc[:l] + g_clone.squeeze()
    dLdrho = g_acc / len(k_list)

    #convert from dLdrho to dLdlogitrho
    rho = model.GetRho_list(1, len(dLdrho))
    drhodlogitrho = rho * (1-rho)
    dLdlogitrho = dLdrho * drhodlogitrho

    return NLL, KL_zreal, KL_beta, KL_discrete, dLdlogitrho


def nll_and_kl(recon_x, x, log_likelihood, a, b, logsample, z_discrete, logit_post, log_prior, mu, logvar, dataset_size, args=Namespace(), test=False):
    batch_size = x.size()[0]
    NLL = -1 * log_likelihood(recon_x, x)
    KL_zreal = -0.5 * (1. + logvar - mu**2 - logvar.exp())
    KL_beta = kl_divergence(a, b, prior_alpha=args.alpha0, log_beta_prior=np.log(1./args.alpha0), args=args).repeat(batch_size, 1) * (1. / dataset_size)

    # in test mode, our samples are essentially coming from a Bernoulli
    if not test:
        KL_discrete = kl_discrete(logit_post, logit(log_prior.exp()), logsample, args.temp, args.temp_prior)
        # global multiplier 1000
        KL_beta = KL_beta * 1000
    else:
        pi_prior = torch.exp(log_prior)
        pi_posterior = torch.sigmoid(logit_post)
        kl_1 = z_discrete * (pi_posterior + SMALL).log() + (1 - z_discrete) * (1 - pi_posterior + SMALL).log()
        kl_2 = z_discrete * (pi_prior + SMALL).log() + (1 - z_discrete) * (1 - pi_prior + SMALL).log()
        KL_discrete = kl_1 - kl_2

    return NLL, KL_zreal, KL_beta, KL_discrete

def elbo(weight, one_minus_rho_weighted, recon_x, x, log_likelihood, a, b, logsample, z_discrete, logit_post, log_prior, mu, logvar, dataset_size, k_list, model, args=Namespace(), test=False):
    if not test:
        NLL, KL_zreal, KL_beta, KL_discrete, grad_rho = nll_and_kl_rrs(weight, one_minus_rho_weighted, recon_x,  x, log_likelihood, a, b, logsample, z_discrete, logit_post, log_prior, mu, logvar, dataset_size, k_list, model, args, test=test)
    else:
        NLL, KL_zreal, KL_beta, KL_discrete = nll_and_kl(recon_x,  x, log_likelihood, a, b, logsample, z_discrete, logit_post, log_prior, mu, logvar, dataset_size, args, test=test)


    return NLL + KL_zreal + KL_beta + KL_discrete, (NLL, KL_zreal, KL_beta, KL_discrete), grad_rho

# def iwae_loss(recon_x, x, log_likelihood, a, b, logsample, z_discrete, logit_post, log_prior, mu, logvar, args, num_samples, dataset_size):
#     # no KL_beta
#     NLL, KL_zreal, _, KL_discrete = nll_and_kl(recon_x, x, log_likelihood, a, b, logsample, z_discrete, logit_post, log_prior, mu, logvar, dataset_size, args, test=True)
#
#     # everything is [num_samples x batch_size] after these lines
#     NLL = torch.stack(torch.chunk(NLL, num_samples, 0)).sum(2)
#     KL_zreal = torch.stack(torch.chunk(KL_zreal, num_samples, 0)).sum(2)
#     KL_discrete = torch.stack(torch.chunk(KL_discrete, num_samples, 0)).sum(2)
#
#     # note: we drop the KL_beta, and add it later
#     return (NLL + KL_discrete + KL_zreal).sum(1)

def train(train_loader, model, log_likelihood, optimizer, epoch, args=Namespace()):

    model.train()
    model.double()
    train_loss = 0.
    for batch_idx, (data, zs) in enumerate(train_loader):
        data = Variable(data.double(), requires_grad=False)
        if args.cuda:
            data = data.cuda()

        weight, one_minus_rho_weighted, recon_x, logsample, logit_post, log_prior, mu, logvar, z_discrete, z_continuous, k_list = model(data)
        loss, (NLL, KL_zreal, KL_beta, KL_discrete), grad_rho = elbo(weight, one_minus_rho_weighted, recon_x, data, log_likelihood,
                F.softplus(model.beta_a) + 0.01, F.softplus(model.beta_b) + 0.01,
                logsample, z_discrete, logit_post, log_prior, mu, logvar, len(train_loader.dataset), k_list, model, args)

        optimizer.zero_grad()
        loss.backward()
        train_loss += loss.data[0]
        optimizer.step()

        # update rho
        grad_rho = grad_rho.data
        if len(grad_rho) < model.L:
            grad_rho = torch.cat([grad_rho, torch.zeros(model.L - len(grad_rho)).type(torch.DoubleTensor).cuda()])
        model.logitrho.data.add_(-1*1.5*args.lr, grad_rho)


        if batch_idx % args.log_interval == 0 and epoch % args.log_epoch == 0:
            print_in_epoch_summary(epoch, batch_idx, len(data), len(train_loader.dataset), loss.data[0], NLL.sum().data[0],
                    {'zreal': KL_zreal.sum().data[0], 'beta': KL_beta.sum().data[0], 'discrete': KL_discrete.sum().data[0]})

    if epoch % args.log_epoch == 0:
        print_epoch_summary(epoch, train_loss / len(train_loader.dataset))
    return train_loss / len(train_loader.dataset)

def test(test_loader, model, log_likelihood, epoch, mode='validation', args=Namespace(), optimizer=None, num_samples=None):
    model.eval()
    #model.double()
    test_loss = 0
    for batch_idx, (data, _) in enumerate(test_loader):
        #data = Variable(data.double(), requires_grad=False)
        data = Variable(data, requires_grad=False)
        if args.cuda:
            data = data.cuda()

        recon_batch, logsample, logit_post, log_prior, mu, logvar, z_discrete, z_continuous = model(data)
        loss, (NLL, KL_zreal, KL_beta, KL_discrete), grad_rho = elbo(recon_batch, data, log_likelihood,
                F.softplus(model.beta_a) + 0.01, F.softplus(model.beta_b) + 0.01,
                logsample, z_discrete, logit_post, log_prior, mu, logvar, len(test_loader.dataset), args, test=True)
        test_loss += loss.data[0]

    test_loss /= len(test_loader.dataset)
    print('====> {:<12} ELBO loss: {:.3f}'.format(mode, test_loss))
    sys.stdout.flush()
    return test_loss

# def eval_iwae_loss(test_loader, model, log_likelihood, epoch, num_samples=10, args=Namespace(), mode='validation'):
#
#     def iwae_loss(recon_x, x, log_likelihood, a, b, logsample, z_discrete, logit_post, log_prior, mu, logvar, args, num_samples, dataset_size):
#         # no KL_beta
#         NLL, KL_zreal, _, KL_discrete = nll_and_kl(recon_x, x, log_likelihood, a, b, logsample, z_discrete, logit_post, log_prior, mu, logvar, dataset_size, args, test=True)
#
#         # everything is [num_samples x batch_size] after these lines
#         NLL = torch.stack(torch.chunk(NLL, num_samples, 0)).sum(2)
#         KL_zreal = torch.stack(torch.chunk(KL_zreal, num_samples, 0)).sum(2)
#         KL_discrete = torch.stack(torch.chunk(KL_discrete, num_samples, 0)).sum(2)
#
#         # note: we drop the KL_beta, and add it later
#         return (NLL + KL_discrete + KL_zreal).sum(1)
#
#
#     model.eval()
#     model.double()
#
#     iwae_loss_list = []
#
#     # initialize the k-set of losses, and sample the stick breaking weights
#     size = args.batch_size * num_samples
#     log_priors = reparametrize(
#         (F.softplus(model.beta_a) + 0.01).view(1, model.truncation).expand(size, model.truncation),
#         (F.softplus(model.beta_b) + 0.01).view(1, model.truncation).expand(size, model.truncation),
#         ibp=True, log=True)
#
#     for batch_idx, (data, _) in enumerate(test_loader):
#         data = Variable(data.double(), requires_grad=False)
#         if args.cuda:
#             data = data.cuda()
#         data = data.repeat(num_samples, 1)
#
#         recon_batch, logsample, logit_post, log_prior, mu, logvar, z_discrete, z_continuous = model(data, log_priors[:data.size(0)])
#         batch_loss = iwae_loss(recon_batch, data, log_likelihood, F.softplus(model.beta_a) + 0.01, F.softplus(model.beta_b) + 0.01,  logsample, z_discrete, logit_post, log_prior, mu, logvar, args, num_samples, len(test_loader.dataset))
#         losses = batch_loss.data
#
#         kl_beta = kl_divergence(F.softplus(model.beta_a) + 0.01, F.softplus(model.beta_b) + 0.01, prior_alpha=args.alpha0, log_beta_prior=np.log(1./args.alpha0), args=args) * args.batch_size / len(test_loader.dataset)
#         losses += kl_beta.data.sum()
#
#         iwae_loss_batch = -(log_sum_exp(-losses, dim=0)) + np.log(num_samples)
#         iwae_loss_batch = iwae_loss_batch * (1. / args.batch_size)
#         iwae_loss_list += [iwae_loss_batch[0]]
#
#
#     print('====> Importance weighted {:<12} loss (n={}): {:.3f}'.format(mode, num_samples, np.array(iwae_loss_list).mean()))
#     sys.stdout.flush()
#     return np.array(iwae_loss_list).mean()




def eval_iwae_loss(test_loader, model, log_likelihood, epoch, num_samples=10, args=Namespace(), mode='validation'):

    def iwae_loss(recon_x, x, log_likelihood, a, b, logsample, z_discrete, logit_post, log_prior, mu, logvar, args, num_samples, dataset_size):
        # no KL_beta
        NLL, KL_zreal, _, KL_discrete = nll_and_kl(recon_x, x, log_likelihood, a, b, logsample, z_discrete, logit_post, log_prior, mu, logvar, dataset_size, args, test=True)

        # everything is [num_samples x batch_size] after these lines
        NLL = torch.stack(torch.chunk(NLL, num_samples, 0)).sum(2)
        KL_zreal = torch.stack(torch.chunk(KL_zreal, num_samples, 0)).sum(2)
        KL_discrete = torch.stack(torch.chunk(KL_discrete, num_samples, 0)).sum(2)

        # note: we drop the KL_beta, and add it later
        return (NLL + KL_discrete + KL_zreal).sum(1)


    model.eval()
    model.double()

    # compute k=E[m]
    k_list = model.SampleTau(m=1000)
    k = int(np.floor((np.asarray(k_list).mean())))

    # initialize the k-set of losses, and sample the stick breaking weights
    losses = torch.zeros(num_samples).double().cuda()
    size = args.batch_size * num_samples
    log_priors = reparametrize(
        (F.softplus(model.beta_a[:k]) + 0.01).view(1, k).expand(size, k),
        (F.softplus(model.beta_b[:k]) + 0.01).view(1, k).expand(size, k),
        ibp=True, log=True)



    for batch_idx, (data, _) in enumerate(test_loader):
        data = Variable(data.double(), requires_grad=False)
        if args.cuda:
            data = data.cuda()
        data = data.repeat(num_samples, 1)

        recon_batch, logsample, logit_post, log_prior, mu, logvar, z_discrete, z_continuous = model(data, log_priors[:data.size(0)], k, test=True)
        batch_loss = iwae_loss(recon_batch, data, log_likelihood, F.softplus(model.beta_a[:k]) + 0.01, F.softplus(model.beta_b[:k]) + 0.01,  logsample, z_discrete, logit_post, log_prior, mu, logvar, args, num_samples, len(test_loader.dataset))
        losses += batch_loss.data



    kl_beta = kl_divergence(F.softplus(model.beta_a[:k]) + 0.01, F.softplus(model.beta_b[:k]) + 0.01, prior_alpha=args.alpha0, log_beta_prior=np.log(1./args.alpha0), args=args)
    losses += kl_beta.data.sum()

    iwae_loss = (-(log_sum_exp(-losses, dim=0)) + np.log(num_samples)) / len(test_loader.dataset)

    print('====> Importance weighted {:<12} loss (n={}): {:.3f}'.format(mode, num_samples, iwae_loss[0]))
    sys.stdout.flush()


    z_list = []
    # compute K
    for batch_idx, (data, _) in enumerate(test_loader):
        data = Variable(data.double(), requires_grad=False)
        if args.cuda:
            data = data.cuda()

        recon_batch, logsample, logit_post, log_prior, mu, logvar, z_discrete, z_continuous = model(data,
                                                                                                    log_priors[
                                                                                                    :data.size(0)],
                                                                                                    k, test=True)
        z_list.append(z_discrete.data.cpu())

    for i in range(len(z_list)):
        if z_list[i].size(-1) < model.k_max:
            z_list[i] = torch.cat([z_list[i], torch.zeros(z_list[i].size(0), model.k_max - z_list[i].size(-1)).type(torch.DoubleTensor)], -1)

    Z = torch.stack(z_list, 0).view(len(test_loader.dataset), -1)
    K = torch.sum(torch.sum((Z > 0.01), 0) > 0)

    print('====> num of activated features (K) : {}'.format(K))

    return iwae_loss[0], K

