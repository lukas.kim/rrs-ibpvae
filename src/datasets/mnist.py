import numpy as np
import torch
import torch.utils.data
import os
import glob
from PIL import Image
from urllib.request import urlretrieve

np.random.seed(3435)


class Mnist(torch.utils.data.Dataset):
    """
    The same dataset used in RAVE
    """

    def __init__(self, root, mode='train', transform=None):
        self.root = os.path.expanduser(root)
        self.mode = mode
        self.transform = transform
        # self.root = 'data'

        if not self._check_exists():
            raise RuntimeError('Dataset not found; try download=True')

        if self.mode == 'train':
            path = os.path.join(self.root, "mnist/train.npy")
            self.train_data = torch.FloatTensor(np.load(path)).transpose(0,1)
        elif self.mode == 'test':
            path = os.path.join(self.root, "mnist/test.npy")
            self.test_data = torch.FloatTensor(np.load(path)).transpose(0,1)
        elif self.mode == 'valid':
            path = os.path.join(self.root, "mnist/test.npy")
            self.valid_data = torch.FloatTensor(np.load(path)).transpose(0,1)

    def _check_exists(self):
        if self.mode == 'train':
            return os.path.exists(os.path.join(self.root, 'mnist/train.npy'))
        elif self.mode == 'test':
            return os.path.exists(os.path.join(self.root, 'mnist/test.npy'))
        elif self.mode == 'valid':
            return os.path.exists(os.path.join(self.root, 'mnist/test.npy'))

    def __getitem__(self, index):
        if self.mode == 'train':
            img = self.train_data[index]
        elif self.mode == 'test':
            img = self.test_data[index]
        elif self.mode == 'valid':
            img = self.valid_data[index]

        if self.transform is not None:
            pass
            # img = self.transform(img)
        else:
            # doing this so that it is consistent with all other datasets
            # to return a PIL Image
            img = Image.fromarray(img, mode='L')

        # dummy for label
        return img, ()

    def __len__(self):
        if self.mode == 'train':
            return len(self.train_data)
        elif self.mode == 'test':
            return len(self.test_data)
        elif self.mode == 'valid':
            return len(self.valid_data)
