import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import sys
import numpy as np
import pdb
from utils import *

from .common import init_weights, reparametrize, reparametrize_discrete, reparametrize_gaussian, flip

SMALL = 1e-16

class RRS_IBP_Concrete(nn.Module):
    def __init__(self,  k_max=100, temp=1., alpha0=5., dataset='mnist', hidden=500):
        super(RRS_IBP_Concrete, self).__init__()
        self.temp = temp
        self.alpha0 = alpha0
        self.dataset = dataset
        self.hidden = hidden
        self.k_max = k_max

        self.L = 1
        self.fc1_encode = nn.Linear(784, self.hidden) # I --> x
        self.fc2_encode_distZ = nn.Linear(self.hidden, self.k_max)
        self.fc2_encode_distA_mu = nn.Linear(self.hidden, self.k_max)
        self.fc2_encode_distA_sig = nn.Linear(self.hidden, self.k_max)



        a_val = np.log(np.exp(self.alpha0) - 1)  # inverse softplus
        b_val = np.log(np.exp(1.) - 1)
        self.beta_a = nn.Parameter(torch.DoubleTensor(self.k_max).zero_() + a_val)
        self.beta_b = nn.Parameter(torch.DoubleTensor(self.k_max).zero_() + b_val)

        # generate: deep
        self.fc1_decode = nn.Linear(self.k_max, self.hidden)
        self.fc2_decode = nn.Linear(self.hidden, 784)

        init_weights([self.fc1_encode,
                      self.fc2_encode_distZ,
                      self.fc2_encode_distA_mu,
                      self.fc2_encode_distA_sig,
                      self.fc1_decode,
                      self.fc2_decode])

        # rho
        self.logitrho = nn.Parameter(torch.DoubleTensor(self.L).zero_())



    def SampleTau(self, m=10):
        tau_list = []
        # sample m number of taus
        for n in range(m):
            t = 1
            while True:
                u = torch.rand(1).type(torch.DoubleTensor)
                temp = self.GetRho(t).cpu()

                if (u > temp).numpy():  # (1-rho)
                    tau_list += [t]
                    break
                else:
                    t = t + 1
        return tau_list

    def GetRho(self, t):
        L = self.L

        if t > L:
            # increase L & lazy initialization of variational parameters and rho
            self.L = t

            self.logitrho = nn.Parameter(torch.cat([self.logitrho.data, torch.DoubleTensor(t-L).zero_().cuda()]))
            # a_val = np.log(np.exp(self.alpha0) - 1)  # inverse softplus
            # b_val = np.log(np.exp(1.) - 1)
            # self.beta_a = nn.Parameter(torch.cat([self.beta_a.data, torch.Tensor(t-L).zero_() + a_val]))
            # self.beta_b = nn.Parameter(torch.cat([self.beta_b.data, torch.Tensor(t-L).zero_() + b_val]))


        return torch.sigmoid(self.logitrho[t-1].data)

    def GetRho_list(self, t_start, t_end):
        return torch.sigmoid(self.logitrho[t_start-1:t_end])


    def LinearLazy(self, x, layer, k): # layer.weight: output x input, layer.bias: output
        # x: batch_size x in_channel
        # layer.weight: out_channel x in_channel
        weight = layer.weight
        bias = layer.bias

        if weight.size(1) == x.size(1) and weight.size(0) > k:
            weight = weight[:k, :]
            bias = bias[:k]
        elif weight.size(1) > x.size(1):
            weight = weight[:, :x.size(1)]
            #bias = bias[:x.size(1)]

        return F.linear(x, weight, bias)

    def encode(self, x, k):
        feat = F.relu(self.fc1_encode(x))
        return self.LinearLazy(feat, self.fc2_encode_distZ, k), \
               self.LinearLazy(feat, self.fc2_encode_distA_mu, k), \
               self.LinearLazy(feat, self.fc2_encode_distA_sig, k)

    def init_bias(self, loader):
        if self.cuda:
            bias = torch.cuda.DoubleTensor(784).zero_()
        else:
            bias = torch.DoubleTensor(784).zero_()
        for batch_idx, (data, _) in enumerate(loader):
            new_sum = data.sum(0)
            if self.cuda:
                new_sum = new_sum.cuda()
            bias += new_sum
        bias = bias / len(loader.dataset)
        self.set_bias(torch.log(bias + SMALL) - torch.log(1. - bias + SMALL))

    def set_bias(self, bias):
        self.fc3_decode.bias.data.copy_(bias)

    def set_temp(self, temp):
        self.temp = temp

    def decode(self, z_discrete):
        self.LinearLazy(z_discrete, self.fc1_decode, z_discrete.size(1))
        return F.sigmoid(self.fc2_decode(
            F.relu(self.LinearLazy(z_discrete, self.fc1_decode, z_discrete.size(1)))
            )
        )

    def forward(self, x, log_prior=None, k=100, m=10, test=False):

        if not test:
            # Roulette
            k_list = self.SampleTau(m=m)

            #print(k_list)
            #print(self.logitrho)

            k_max = int(np.max(k_list))
            if (k_max > self.k_max):
                self.k_max = k_max

            # Compute weights for efficient multi-sample RRS
            rho = self.GetRho_list(t_start=1, t_end=k_max)
            one_minus_rho = 1 - rho
            one_minus_rho_weighted = Variable(torch.zeros(k_max).type(torch.cuda.DoubleTensor)).cuda()
            # Multiple sample re-weighting
            temp = one_minus_rho.clone()
            for k in k_list:
                one_minus_rho_weighted[:k] = one_minus_rho_weighted[:k] + temp[:k]

            one_minus_rho_weighted = one_minus_rho_weighted * (1./m)

            # NOTE: below takes use the independent property of KLs
            # .     cumsum due to the fact that R(i) includes all R(j<i)∂
            weight = flip(torch.cumsum(flip(one_minus_rho_weighted), 0))

            batch_size = x.size(0)
            beta_a = F.softplus(self.beta_a[:k_max]) + 0. # f(x) = log(1+exp(x))
            beta_b = F.softplus(self.beta_b[:k_max]) + 0.01

            # might be passed in for IWAE
            if log_prior is None:
                log_prior = reparametrize(
                    beta_a.view(1, k_max).expand(batch_size, k_max),
                    beta_b.view(1, k_max).expand(batch_size, k_max),
                    ibp=True, log=True) # outputs the cumulative sum of log \nu_k

            logit_x, mu, logvar = self.encode(x.view(-1, 784), k=k_max) # batch_size x 100 (for logit_x, mu, logvar)
            logit_post = logit_x + logit(log_prior.exp()) # equation (6) of RAVE ; #  logit(x) := (x + SMALL).log() - (1. - x + SMALL).log()

            logsample = reparametrize_discrete(logit_post, self.temp)
            z_discrete = F.sigmoid(logsample) # binary
            z_continuous = reparametrize_gaussian(mu, logvar) # A

            # zero-temperature rounding
            if not self.training:
                z_discrete = torch.round(z_discrete)

            recon_x = []
            code = z_discrete * z_continuous
            for k in range(k_max):
                recon_x += [self.decode(code[:, :k+1])]

            return weight, one_minus_rho_weighted, recon_x, logsample, logit_post, log_prior, mu, logvar, z_discrete, z_continuous, k_list

        else: # test mode
            batch_size = x.size(0)

            logit_x, mu, logvar = self.encode(x.view(-1, 784), k=k)  # batch_size x 100 (for logit_x, mu, logvar)
            logit_post = logit_x + logit(
                log_prior.exp())  # equation (6) of RAVE ; #  logit(x) := (x + SMALL).log() - (1. - x + SMALL).log()

            logsample = reparametrize_discrete(logit_post, self.temp)
            z_discrete = F.sigmoid(logsample)  # binary
            z_continuous = reparametrize_gaussian(mu, logvar)  # A

            # zero-temperature rounding
            if not self.training:
                z_discrete = torch.round(z_discrete)


            return self.decode(
                z_discrete * z_continuous), logsample, logit_post, log_prior, mu, logvar, z_discrete, z_continuous

            # self.decode(z_discrete * z_continuous): \mu_\theta (z x A)
            # logsample: sigmoid^-1 (z)
            # logit_post: logit(\pi_k) + \phi_k x_n
            # log_prior: \sum^k log(\nu_k):= log \phi_k
            # mu: \mu(X_n) for q_\phi(A_n|x_n)
            # logvar: \log \sigma^2(x_n) for q_\phi(A_n|x_n)
            # z_discrete: z
            # z_continuous: A


